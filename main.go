package main

import (
	"fmt"
	"log"
	"time"

	"github.com/Difrex/gosway/ipc"
	"gitlab.com/rmc/launchpad"
)

const (
	workspaceCount       = 10
	rowCount             = 2
	workspacePerRowCount = workspaceCount / rowCount
	longPressDuration    = 300 * time.Millisecond
)

func main() {
	btnPresses := make([]chan bool, workspaceCount+1)

	pad, err := launchpad.Connect()
	if err != nil {
		log.Fatalf("Error connecting to launchpad: %v", err)
	}
	defer pad.SendMessage(launchpad.ResetMessage)

	swayCommandConnection, err := ipc.NewSwayConnection()
	if err != nil {
		panic(err)
	}
	swaySubConnection, err := ipc.NewSwayConnection()
	if err != nil {
		panic(err)
	}

	err = lightWorkspaces(pad, swayCommandConnection)
	if err != nil {
		panic(err)
	}
	_, err = swaySubConnection.SendCommand(ipc.IPC_SUBSCRIBE, `["workspace"]`)
	if err != nil {
		panic(err)
	}
	ss := swaySubConnection.Subscribe()
	defer ss.Close()

	ls, err := pad.Subscribe()
	defer ls.Stop()
	if err != nil {
		panic(err)
	}

	handlePress := func(m *launchpad.ButtonPressedMessage) {
		w := rowColToWorkspace(m.Row, m.Col)
		if w == -1 {
			return
		}
		if m.On {
			if (btnPresses[w] != nil) {
				close(btnPresses[w])
			}
			btnPresses[w] = make(chan bool, 1)
			go func(c chan bool) {
				select {
				case <-c:
					swayCommandConnection.RunSwayCommand(fmt.Sprintf("workspace number %d", w))
				case <-time.After(longPressDuration):
					swayCommandConnection.RunSwayCommand(fmt.Sprintf("move container to workspace number %d", w))
				}

			}(btnPresses[w])
		} else {
			btnPresses[w] <- true
		}
	}

	for {
		select {
		case <-ss.Events:
			err = lightWorkspaces(pad, swayCommandConnection)
			if err != nil {
				log.Fatal(err)
			}
		case err := <-ss.Errors:
			log.Fatal(err)
		case m := <-ls.Messages:
			handlePress(m)
		}
	}
}

func lightWorkspaces(pad *launchpad.Launchpad, sc *ipc.SwayConnection) error {
	err := pad.SendMessage(launchpad.ResetMessage)
	if err != nil {
		panic(err)
	}
	wss, err := sc.GetWorkspaces()
	if err != nil {
		return err
	}
	for _, ws := range wss {
		color := launchpad.Color(launchpad.BrightnessFull, launchpad.BrightnessLow, launchpad.NoteOnFlagsNormal)
		if ws.Urgent {
			color = launchpad.Color(0, launchpad.BrightnessFull, launchpad.NoteOnFlagsFlash)
		}
		if ws.Visible {
			color = launchpad.Color(launchpad.BrightnessMedium, 0, launchpad.NoteOnFlagsNormal)
		}
		if ws.Focused {
			color = launchpad.Color(launchpad.BrightnessFull, 0, launchpad.NoteOnFlagsNormal)
		}
		r, c := workspaceToRowCol(ws.Num)
		err = pad.SendMessage(launchpad.NoteOnMessage(launchpad.GridKey(r, c), color))
		if err != nil {
			return err
		}
	}
	return nil
}

func workspaceToRowCol(w int) (r int, c int) {
	return int((w - 1) / workspacePerRowCount), (w - 1) % workspacePerRowCount
}

func rowColToWorkspace(r, c int) int {
	if r < rowCount && c < workspacePerRowCount {
		return c + 1 + (workspacePerRowCount * r)
	}
	return -1
}
