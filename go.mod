module gitlab.com/rmc/sway-launchpad

go 1.21

require github.com/Difrex/gosway/ipc v0.0.0-20230801141530-6213ced34703

require gitlab.com/rmc/launchpad v0.0.0-20240120101510-2345bb5e6b9d

require (
	gitlab.com/gomidi/midi v1.21.0 // indirect
	gitlab.com/gomidi/midi/v2 v2.0.30 // indirect
	gitlab.com/gomidi/rtmididrv v0.13.0 // indirect
)
